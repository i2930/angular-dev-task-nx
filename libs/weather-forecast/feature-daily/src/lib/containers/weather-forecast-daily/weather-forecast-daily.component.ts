import { Component } from '@angular/core';
import { DailyEntity, DailyFacade } from '@nx-test/weather-forecast/feature-daily';
import { Observable } from 'rxjs';

@Component({
  selector: 'nx-test-weather-forecast-daily',
  templateUrl: './weather-forecast-daily.component.html',
  styleUrls: ['./weather-forecast-daily.component.scss']
})
export class WeatherForecastDailyComponent {

  public dailyForecastData: Observable<DailyEntity[]>;
  loaded$: Observable<boolean>;
  dailyError$: Observable<any>;
  constructor(private facade: DailyFacade) {
    this.dailyForecastData = this.facade.allDaily$;
    this.dailyError$ = this.facade.getDailyError$;
    this.loaded$ = this.facade.loaded$;
  }
}
