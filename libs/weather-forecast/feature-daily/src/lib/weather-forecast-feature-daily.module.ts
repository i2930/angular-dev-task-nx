import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromDaily from './+state/daily/daily.reducer';
import { DailyEffects } from './+state/daily/daily.effects';
import { DailyFacade } from './+state/daily/daily.facade';
import { RouterModule, Routes } from '@angular/router';
import { WeatherForecastDailyComponent } from './containers/weather-forecast-daily/weather-forecast-daily.component';
import { WeatherForecastUiModule } from '@nx-test/weather-forecast/ui';

const routes: Routes = [
  {
    path: '',
    component: WeatherForecastDailyComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    StoreModule.forFeature(fromDaily.DAILY_FEATURE_KEY, fromDaily.reducer),
    EffectsModule.forFeature([DailyEffects]),
    WeatherForecastUiModule
  ],
  providers: [DailyFacade],
  declarations: [
    WeatherForecastDailyComponent
  ],
})
export class WeatherForecastFeatureDailyModule {}
