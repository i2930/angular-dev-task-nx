import { Action } from '@ngrx/store';

import * as DailyActions from './daily.actions';
import { DailyEntity } from './daily.models';
import { State, initialState, reducer } from './daily.reducer';

describe('Daily Reducer', () => {
  const createDailyEntity = (id: string, name = ''): DailyEntity => ({
    id,
    name: name || `name-${id}`,
  });

  describe('valid Daily actions', () => {
    it('loadDailySuccess should return the list of known Daily', () => {
      const daily = [
        createDailyEntity('PRODUCT-AAA'),
        createDailyEntity('PRODUCT-zzz'),
      ];
      const action = DailyActions.loadDailySuccess({ daily });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
