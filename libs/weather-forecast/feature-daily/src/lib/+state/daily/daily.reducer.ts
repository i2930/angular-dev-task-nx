import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import * as DailyActions from './daily.actions';
import { DailyEntity } from './daily.models';
import { Coordinates, CoordinatesModel } from '@nx-test/weather-forecast/data-access';

export const DAILY_FEATURE_KEY = 'daily';

export interface State extends EntityState<DailyEntity> {
  selectedId?: any; // which Daily record has been selected
  searchedCityName: string;
  cityCoordinates: Coordinates;
  loaded: boolean; // has the Daily list been loaded
  error?: string | null; // last known error (if any)
}

export interface DailyPartialState {
  readonly [DAILY_FEATURE_KEY]: State;
}

export const dailyAdapter: EntityAdapter<DailyEntity> =
  createEntityAdapter<DailyEntity>();

export const initialState: State = dailyAdapter.getInitialState({
  // set initial required properties
  selectedId: '',
  searchedCityName: '',
  cityCoordinates: new CoordinatesModel(40.0, 40.0),
  loaded: true
});

const dailyReducer = createReducer(
  initialState,
  on(DailyActions.searchCity, (state, { cityName }) => ({
    ...state,
    loaded: false,
    error: null,
    searchedCityName: cityName
  })),
  on(DailyActions.searchCityError, (state, { error }) => ({ ...state, error: error, loaded: true })),
  on(DailyActions.loadDailySuccess, (state, { daily }) =>
    dailyAdapter.setOne(daily, { ...state, loaded: true })
  ),
  on(DailyActions.loadDailyFailure, (state, { error }) => ({ ...state, error: error, loaded: true }))
);

export function reducer(state: State | undefined, action: Action) {
  return dailyReducer(state, action);
}
