import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';

import * as DailyActions from './daily.actions';
import { CoordinatesModel, WeatherForecastHomeService } from '@nx-test/weather-forecast/data-access';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';
import { WeatherDataResponseModel } from '@nx-test/weather-forecast/feature-daily';


@Injectable()
export class DailyEffects {
  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DailyActions.searchCity),
      mergeMap((payload) => this.service.getCityByname(payload.cityName).pipe(
        switchMap((cityData) => {
          const city = cityData[0];
          if (!city) throw new Error('Cities not found');
          const cityName = city.name;
          DailyActions.searchCitySuccess({ city: cityName });
          return this.service.getWeatherData(new CoordinatesModel(city.lat, city.lon), true)
            .pipe(map(dat => ({ ...dat, ...{ cityName } })));
        }),
        map(data => {
          return DailyActions.loadDailySuccess({ daily: new WeatherDataResponseModel(data.cityName, data) });
        }),
        catchError(error => of(DailyActions.searchCityError({ error: error })))
      ))
    )
  );


  constructor(private readonly actions$: Actions, private service: WeatherForecastHomeService) {
  }
}
