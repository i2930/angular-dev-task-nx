import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DAILY_FEATURE_KEY, State, dailyAdapter } from './daily.reducer';

// Lookup the 'Daily' feature state managed by NgRx
export const getDailyState = createFeatureSelector<State>(DAILY_FEATURE_KEY);

const { selectAll, selectEntities } = dailyAdapter.getSelectors();

export const getDailyLoaded = createSelector(
  getDailyState,
  (state: State) => state.loaded
);

export const getDailyError = createSelector(
  getDailyState,
  (state: State) => state.error
);

export const getAllDaily = createSelector(getDailyState, (state: State) =>
  selectAll(state)
);

export const getDailyEntities = createSelector(getDailyState, (state: State) =>
  selectEntities(state)
);


export const getSelectedId = createSelector(
  getDailyState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getDailyEntities,
  getSelectedId,
  (entities, selectedId) => (selectedId ? entities[selectedId] : undefined)
);
