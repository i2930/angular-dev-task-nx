/**
 * Interface for the 'Daily' data
 */
import { kelvinToCelsius } from '../../../../../data-access/src/lib/core/helpers';

export interface DailyEntity {
  id: string,
  cityName: string,
  forecasts: TemperatureEntity[]
}

export interface TemperatureEntity {
  temperature: number,
  unit: string
}


export class WeatherDataResponseModel implements DailyEntity {
  id: string;
  cityName: string;
  forecasts: any[];

  constructor(cityName: string, weatherDataResponse: any) {
    this.id = cityName;
    this.cityName = cityName;
    this.forecasts = this.refactorForecasts(weatherDataResponse);
    return this;
  }

  refactorForecasts(data: any) {
    return data.daily.map((forecast: any) => {
      return { temperature: kelvinToCelsius(forecast.temp.day), unit: 'C' };
    }).filter((_: any, index: any) => index != data.daily.length - 1);
  }
}

