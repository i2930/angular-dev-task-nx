import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import * as DailyActions from './daily.actions';
import { DailyEffects } from './daily.effects';

describe('DailyEffects', () => {
  let actions: Observable<Action>;
  let effects: DailyEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        DailyEffects,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(DailyEffects);
  });

  describe('init$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: DailyActions.init() });

      const expected = hot('-a-|', {
        a: DailyActions.loadDailySuccess({ daily: [] }),
      });

      expect(effects.init$).toBeObservable(expected);
    });
  });
});
