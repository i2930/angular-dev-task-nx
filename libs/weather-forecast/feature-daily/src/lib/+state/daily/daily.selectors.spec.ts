import { DailyEntity } from './daily.models';
import { dailyAdapter, DailyPartialState, initialState } from './daily.reducer';
import * as DailySelectors from './daily.selectors';

describe('Daily Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getDailyId = (it: DailyEntity) => it.id;
  const createDailyEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as DailyEntity);

  let state: DailyPartialState;

  beforeEach(() => {
    state = {
      daily: dailyAdapter.setAll(
        [
          createDailyEntity('PRODUCT-AAA'),
          createDailyEntity('PRODUCT-BBB'),
          createDailyEntity('PRODUCT-CCC'),
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Daily Selectors', () => {
    it('getAllDaily() should return the list of Daily', () => {
      const results = DailySelectors.getAllDaily(state);
      const selId = getDailyId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = DailySelectors.getSelected(state) as DailyEntity;
      const selId = getDailyId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getDailyLoaded() should return the current "loaded" status', () => {
      const result = DailySelectors.getDailyLoaded(state);

      expect(result).toBe(true);
    });

    it('getDailyError() should return the current "error" state', () => {
      const result = DailySelectors.getDailyError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
