import { createAction, props } from '@ngrx/store';
import { DailyEntity } from './daily.models';

export const searchCity = createAction('[Daily Page] Search city', props<{cityName: string}>());
export const searchCityError = createAction('[Daily Page] Search city error', props<{error: string}>());
export const searchCitySuccess = createAction('[Daily Page] Search city success', props<{city: string}>());

export const loadDailySuccess = createAction(
  '[Daily/API] Load Daily Success',
  props<{ daily: DailyEntity }>()
);

export const loadDailyFailure = createAction(
  '[Daily/API] Load Daily Failure',
  props<{ error: any }>()
);
