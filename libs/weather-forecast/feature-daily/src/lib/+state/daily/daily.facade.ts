import { Injectable } from '@angular/core';
import { select, Store, Action } from '@ngrx/store';

import * as DailyActions from './daily.actions';
import * as DailySelectors from './daily.selectors';

@Injectable()
export class DailyFacade {
  /**
   * Combine pieces of state using createSelector,
   * and expose them as observables through the facade.
   */
  loaded$ = this.store.pipe(select(DailySelectors.getDailyLoaded));
  allDaily$ = this.store.pipe(select(DailySelectors.getAllDaily));
  getDailyError$ = this.store.pipe(select(DailySelectors.getDailyError));
  constructor(private readonly store: Store) {}

  /**
   * Use the initialization action to perform one
   * or more tasks in your Effects.
   */
  searchCities(cityName: string) {
    this.store.dispatch(DailyActions.searchCity({cityName}));
  }
}
