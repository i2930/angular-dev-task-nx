import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';
import { NxModule } from '@nrwl/angular';
import { readFirst } from '@nrwl/angular/testing';

import * as DailyActions from './daily.actions';
import { DailyEffects } from './daily.effects';
import { DailyFacade } from './daily.facade';
import { DailyEntity } from './daily.models';
import {
  DAILY_FEATURE_KEY,
  State,
  initialState,
  reducer,
} from './daily.reducer';
import * as DailySelectors from './daily.selectors';

interface TestSchema {
  daily: State;
}

describe('DailyFacade', () => {
  let facade: DailyFacade;
  let store: Store<TestSchema>;
  const createDailyEntity = (id: string, name = ''): DailyEntity => ({
    id,
    name: name || `name-${id}`,
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(DAILY_FEATURE_KEY, reducer),
          EffectsModule.forFeature([DailyEffects]),
        ],
        providers: [DailyFacade],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ],
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.inject(Store);
      facade = TestBed.inject(DailyFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async () => {
      let list = await readFirst(facade.allDaily$);
      let isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(false);

      facade.init();

      list = await readFirst(facade.allDaily$);
      isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(true);
    });

    /**
     * Use `loadDailySuccess` to manually update list
     */
    it('allDaily$ should return the loaded list; and loaded flag == true', async () => {
      let list = await readFirst(facade.allDaily$);
      let isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(false);

      store.dispatch(
        DailyActions.loadDailySuccess({
          daily: [createDailyEntity('AAA'), createDailyEntity('BBB')],
        })
      );

      list = await readFirst(facade.allDaily$);
      isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(2);
      expect(isLoaded).toBe(true);
    });
  });
});
