export interface Coordinates {
	lat: number
	lon: number
}

export class CoordinatesModel implements Coordinates {
	lat: number;
	lon: number;
	constructor(lat: number, lon: number) {
		this.lat = lat;
		this.lon = lon;
		return this;
	}
}

