import { Observable } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { Coordinates } from './models/weather.model';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class WeatherForecastHomeService {
  constructor(
    private http: HttpClient,
    @Inject('weatherApis') private _weatherApis: any,
    @Inject('appKey') private _appKey: any
  ) {
  }

  getCityByname(cityName: string): Observable<any> {
    return this.http.get(this._weatherApis.getCityPath, {
      params: {
        q: cityName,
        limit: 1,
        appid: this._appKey
      }
    });
  }

  getWeatherData(coordinates: Coordinates, isDaily: boolean) {
    return this.http.get(this._weatherApis.getWeatherDataPath, {
      params: {
        lat: coordinates.lat,
        lon: coordinates.lon,
        exclude: `current,minutely,${isDaily ? 'hourly' : 'daily'},alerts`,
        appid: this._appKey
      }
    });
  }
}
