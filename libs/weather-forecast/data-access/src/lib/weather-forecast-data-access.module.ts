import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [CommonModule, 
    HttpClientModule,
  ],
  providers: [],
  exports: [
    HttpClientModule
  ]
})
export class WeatherForecastDataAccessModule {}
