import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HourlyEntity, HourlyFacade } from '@nx-test/weather-forecast/feature-hourly';

@Component({
  selector: 'nx-test-weather-forecast-hourly',
  templateUrl: './weather-forecast-hourly.component.html',
  styleUrls: ['./weather-forecast-hourly.component.scss']
})
export class WeatherForecastHourlyComponent {

  public hourlyForecastData$: Observable<HourlyEntity[]>;
  public hours: string[] = [];
  hourlyError$;
  loaded$;
  constructor(private facade: HourlyFacade) {
    this.hourlyForecastData$ = this.facade.allHourly$;
    this.hourlyError$ = this.facade.getHourlyError$;
    this.loaded$ = this.facade.loaded$;
    this.facade.allHourly$.subscribe((dt) => {
      this.hours = dt[0]?.listHours;
    });
  }
}
