import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromHourly from './+state/hourly/hourly.reducer';
import { HourlyEffects } from './+state/hourly/hourly.effects';
import { HourlyFacade } from './+state/hourly/hourly.facade';
import { RouterModule, Routes } from '@angular/router';
import { WeatherForecastHourlyComponent } from './containers/weather-forecast-hourly/weather-forecast-hourly.component';
import { WeatherForecastUiModule } from '@nx-test/weather-forecast/ui';

const routes: Routes = [
  {
    path: '',
    component: WeatherForecastHourlyComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    StoreModule.forFeature(fromHourly.HOURLY_FEATURE_KEY, fromHourly.reducer),
    EffectsModule.forFeature([HourlyEffects]),
    WeatherForecastUiModule
  ],
  providers: [HourlyFacade],
  declarations: [
    WeatherForecastHourlyComponent
  ],
})
export class WeatherForecastFeatureHourlyModule {}
