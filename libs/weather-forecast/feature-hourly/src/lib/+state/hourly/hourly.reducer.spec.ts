import { Action } from '@ngrx/store';

import * as HourlyActions from './hourly.actions';
import { HourlyEntity } from './hourly.models';
import { State, initialState, reducer } from './hourly.reducer';

describe('Hourly Reducer', () => {
  const createHourlyEntity = (id: string, name = ''): HourlyEntity => ({
    id,
    name: name || `name-${id}`,
  });

  describe('valid Hourly actions', () => {
    it('loadHourlySuccess should return the list of known Hourly', () => {
      const hourly = [
        createHourlyEntity('PRODUCT-AAA'),
        createHourlyEntity('PRODUCT-zzz'),
      ];
      const action = HourlyActions.loadHourlySuccess({ hourly });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as Action;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
