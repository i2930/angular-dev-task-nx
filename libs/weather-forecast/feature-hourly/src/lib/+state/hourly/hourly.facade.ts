import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import * as HourlySelectors from './hourly.selectors';
import * as HourlyActions from '../../../../../feature-hourly/src/lib/+state/hourly/hourly.actions';

@Injectable()
export class HourlyFacade {
  /**
   * Combine pieces of state using createSelector,
   * and expose them as observables through the facade.
   */
  loaded$ = this.store.pipe(select(HourlySelectors.getHourlyLoaded));
  allHourly$ = this.store.pipe(select(HourlySelectors.getAllHourly));
  getHourlyError$ = this.store.pipe(select(HourlySelectors.getHourlyError));

  constructor(private readonly store: Store) {
  }

  /**
   * Use the initialization action to perform one
   * or more tasks in your Effects.
   */
  init() {
    // this.store.dispatch(HourlyActions.init());
  }

  searchCities(cityName: string) {
    this.store.dispatch(HourlyActions.searchCity({ cityName }));
  }
}
