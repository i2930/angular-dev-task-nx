import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';
import { NxModule } from '@nrwl/angular';
import { readFirst } from '@nrwl/angular/testing';

import * as HourlyActions from './hourly.actions';
import { HourlyEffects } from './hourly.effects';
import { HourlyFacade } from './hourly.facade';
import { HourlyEntity } from './hourly.models';
import {
  HOURLY_FEATURE_KEY,
  State,
  initialState,
  reducer,
} from './hourly.reducer';
import * as HourlySelectors from './hourly.selectors';

interface TestSchema {
  hourly: State;
}

describe('HourlyFacade', () => {
  let facade: HourlyFacade;
  let store: Store<TestSchema>;
  const createHourlyEntity = (id: string, name = ''): HourlyEntity => ({
    id,
    name: name || `name-${id}`,
  });

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(HOURLY_FEATURE_KEY, reducer),
          EffectsModule.forFeature([HourlyEffects]),
        ],
        providers: [HourlyFacade],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ],
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.inject(Store);
      facade = TestBed.inject(HourlyFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async () => {
      let list = await readFirst(facade.allHourly$);
      let isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(false);

      facade.init();

      list = await readFirst(facade.allHourly$);
      isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(true);
    });

    /**
     * Use `loadHourlySuccess` to manually update list
     */
    it('allHourly$ should return the loaded list; and loaded flag == true', async () => {
      let list = await readFirst(facade.allHourly$);
      let isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(0);
      expect(isLoaded).toBe(false);

      store.dispatch(
        HourlyActions.loadHourlySuccess({
          hourly: [createHourlyEntity('AAA'), createHourlyEntity('BBB')],
        })
      );

      list = await readFirst(facade.allHourly$);
      isLoaded = await readFirst(facade.loaded$);

      expect(list.length).toBe(2);
      expect(isLoaded).toBe(true);
    });
  });
});
