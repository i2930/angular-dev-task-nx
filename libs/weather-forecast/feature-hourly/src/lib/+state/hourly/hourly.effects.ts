import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import * as HourlyActions from '../../../../../feature-hourly/src/lib/+state/hourly/hourly.actions';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';
import { CoordinatesModel, WeatherForecastHomeService } from '@nx-test/weather-forecast/data-access';
import { WeatherDataHourlyResponseModel } from '@nx-test/weather-forecast/feature-hourly';

@Injectable()
export class HourlyEffects {
  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HourlyActions.searchCity),
      mergeMap((payload) => this.service.getCityByname(payload.cityName).pipe(
        switchMap((cityData) => {
          const city = cityData[0];
          if (!city) throw new Error('Cities not found');
          const cityName = city.name;
          HourlyActions.searchCitySuccess({ city: cityName });
          return this.service.getWeatherData(new CoordinatesModel(city.lat, city.lon), false)
            .pipe(map(dat => ({ ...dat, ...{ cityName } })));
        }),
        map(data => {
          return HourlyActions.loadHourlySuccess({ hourly: new WeatherDataHourlyResponseModel(data.cityName, data) });
        }),
        catchError(error => of(HourlyActions.searchCityError({ error })))
      ))
    )
  );

  constructor(private readonly actions$: Actions, private service: WeatherForecastHomeService) {
  }
}
