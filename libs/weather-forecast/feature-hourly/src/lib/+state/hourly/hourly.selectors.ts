import { createFeatureSelector, createSelector } from '@ngrx/store';
import { HOURLY_FEATURE_KEY, State, hourlyAdapter } from './hourly.reducer';

// Lookup the 'Hourly' feature state managed by NgRx
export const getHourlyState = createFeatureSelector<State>(HOURLY_FEATURE_KEY);

const { selectAll, selectEntities } = hourlyAdapter.getSelectors();

export const getHourlyLoaded = createSelector(
  getHourlyState,
  (state: State) => state.loaded
);

export const getHourlyError = createSelector(
  getHourlyState,
  (state: State) => state.error
);

export const getAllHourly = createSelector(getHourlyState, (state: State) =>
  selectAll(state)
);

export const getHourlyEntities = createSelector(
  getHourlyState,
  (state: State) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getHourlyState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getHourlyEntities,
  getSelectedId,
  (entities, selectedId) => (selectedId ? entities[selectedId] : undefined)
);
