import { createAction, props } from '@ngrx/store';
import { HourlyEntity } from './hourly.models';

export const searchCity = createAction('[Hourly Page] Search city', props<{cityName: string}>());
export const searchCityError = createAction('[Hourly Page] Search city error', props<{error: string}>());
export const searchCitySuccess = createAction('[Hourly Page] Search city success', props<{city: string}>());

export const loadHourlySuccess = createAction(
  '[Hourly/API] Load Hourly Success',
  props<{ hourly: HourlyEntity }>()
);

export const loadHourlyFailure = createAction(
  '[Hourly/API] Load Hourly Failure',
  props<{ error: any }>()
);
