import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import * as HourlyActions from './hourly.actions';
import { HourlyEffects } from './hourly.effects';

describe('HourlyEffects', () => {
  let actions: Observable<Action>;
  let effects: HourlyEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        HourlyEffects,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(HourlyEffects);
  });

  describe('init$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: HourlyActions.init() });

      const expected = hot('-a-|', {
        a: HourlyActions.loadHourlySuccess({ hourly: [] }),
      });

      expect(effects.init$).toBeObservable(expected);
    });
  });
});
