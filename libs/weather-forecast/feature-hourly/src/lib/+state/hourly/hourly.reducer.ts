import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import * as HourlyActions from './hourly.actions';
import { HourlyEntity } from './hourly.models';
import { Coordinates, CoordinatesModel } from '@nx-test/weather-forecast/data-access';

export const HOURLY_FEATURE_KEY = 'hourly';

export interface State extends EntityState<HourlyEntity> {
  selectedId?: string | number; // which Daily record has been selected
  cityCoordinates: Coordinates;
  loaded: boolean; // has the Daily list been loaded
  error?: string | null; // last known error (if any)
}

export interface HourlyPartialState {
  readonly [HOURLY_FEATURE_KEY]: State;
}

export const hourlyAdapter: EntityAdapter<HourlyEntity> =
  createEntityAdapter<HourlyEntity>();

export const initialState: State = hourlyAdapter.getInitialState({
  // set initial required properties
  cityCoordinates: new CoordinatesModel(40.0, 40.0),
  loaded: true
});

const hourlyReducer = createReducer(
  initialState,
  on(HourlyActions.searchCity, (state, { cityName }) => ({
    ...state,
    loaded: false,
    error: null,
  })),
  on(HourlyActions.searchCityError, (state, { error }) => ({ ...state,  loaded: true, error })),
  on(HourlyActions.loadHourlySuccess, (state, { hourly }) =>
    hourlyAdapter.setOne(hourly, { ...state, loaded: true })
  ),
  on(HourlyActions.loadHourlyFailure, (state, { error }) => ({
    ...state,
    error,
    loaded: true
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return hourlyReducer(state, action);
}
