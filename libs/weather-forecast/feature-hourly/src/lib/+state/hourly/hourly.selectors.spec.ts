import { HourlyEntity } from './hourly.models';
import {
  hourlyAdapter,
  HourlyPartialState,
  initialState,
} from './hourly.reducer';
import * as HourlySelectors from './hourly.selectors';

describe('Hourly Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getHourlyId = (it: HourlyEntity) => it.id;
  const createHourlyEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as HourlyEntity);

  let state: HourlyPartialState;

  beforeEach(() => {
    state = {
      hourly: hourlyAdapter.setAll(
        [
          createHourlyEntity('PRODUCT-AAA'),
          createHourlyEntity('PRODUCT-BBB'),
          createHourlyEntity('PRODUCT-CCC'),
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Hourly Selectors', () => {
    it('getAllHourly() should return the list of Hourly', () => {
      const results = HourlySelectors.getAllHourly(state);
      const selId = getHourlyId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = HourlySelectors.getSelected(state) as HourlyEntity;
      const selId = getHourlyId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getHourlyLoaded() should return the current "loaded" status', () => {
      const result = HourlySelectors.getHourlyLoaded(state);

      expect(result).toBe(true);
    });

    it('getHourlyError() should return the current "error" state', () => {
      const result = HourlySelectors.getHourlyError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
