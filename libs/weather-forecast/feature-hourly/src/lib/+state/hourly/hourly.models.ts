/**
 * Interface for the 'Hourly' data
 */
import { kelvinToCelsius } from '../../../../../data-access/src/lib/core/helpers';

export interface HourlyEntity {
  id: string
  cityName: string
  listHours: string[]
  forecasts: TemperatureEntity[]
}

export interface TemperatureEntity {
  id: '',
  temperature: number,
  unit: string
}

export class WeatherDataHourlyResponseModel implements HourlyEntity {
  id: string;
  cityName: string;
  listHours: string[];
  forecasts: any[];

  constructor(cityName: string, weatherDataResponse: any) {
    this.id = cityName;
    this.cityName = cityName;
    this.listHours = this.getHours(weatherDataResponse);
    this.forecasts = this.refactorForecasts(weatherDataResponse);
    console.log(this);
    return this;
  }

  refactorForecasts(data: any) {
    return data.hourly.map((forecast: any) => {
      return { temperature: kelvinToCelsius(forecast.temp) , unit: 'C' };
    }).filter((_: any, i: any) => i % 3 === 0 && i < 24);
  }

  getHours(data: any) {
    return data.hourly.map((forecast: any) => {
      const localTime: any = forecast.dt + data.timezone_offset;
      return new Date(localTime * 1000).getUTCHours() + ':00';
    }).filter((_: any, i: any) => i % 3 === 0 && i < 24);
  }

}
