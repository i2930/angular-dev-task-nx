import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherForecastSearchComponent } from './containers/weather-forecast-search/weather-forecast-search.component';
import { WeatherForecastUiModule } from '@nx-test/weather-forecast/ui';
import { RouterModule, Routes } from '@angular/router';
import { DailyFacade } from '@nx-test/weather-forecast/feature-daily';
import { HourlyFacade } from '@nx-test/weather-forecast/feature-hourly';


const routes: Routes = [
  {
    path: '',
    component: WeatherForecastSearchComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
     WeatherForecastUiModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WeatherForecastSearchComponent],
  providers: [DailyFacade, HourlyFacade],
  exports: [WeatherForecastSearchComponent]
})
export class WeatherForecastFeatureSearchModule {}
