import { Component, OnInit } from '@angular/core';
import { DailyFacade } from '@nx-test/weather-forecast/feature-daily';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { HourlyFacade } from '@nx-test/weather-forecast/feature-hourly';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'nx-test-weather-forecast-search',
  templateUrl: './weather-forecast-search.component.html',
  styleUrls: ['./weather-forecast-search.component.scss']
})
export class WeatherForecastSearchComponent implements OnInit {

  cityName: string = '';
  isDaily: boolean = true;
  private searchSubject$: Subject<string> = new Subject<string>();

  constructor(
    private dailyFacade: DailyFacade,
    private hourlyFacade: HourlyFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {

  }

  ngOnInit(): void {
    this.getCurrentStateOfSearchForm();
    this.listenQueryParams();
    this.listenSearch();
  }

  public listenQueryParams(): void {
    this.activatedRoute.queryParams.subscribe((params: any) => {
      this.cityName = params?.city ? params.city : '';
      if (!this.cityName) return;
      this.isDaily ? this.dailyFacade.searchCities(this.cityName) : this.hourlyFacade.searchCities(this.cityName); // dispatch search
    });
  }

  listenSearch(): void {
    this.searchSubject$.pipe(
      debounceTime(400)
    ).subscribe((cityName) => {
      if (cityName) this.setQueryToUrl(cityName);
    });
  }

  search(cityData: any) {
    this.searchSubject$.next(cityData.target.value);
  }

  switchTable(isDaily: boolean) {
    this.isDaily = isDaily;
    this.router.navigate([this.isDaily ? '/daily' : '/hourly']).then();
  }

  getCurrentStateOfSearchForm() {
    this.isDaily = this.getFirstSegmentOfUrl() === 'daily';
  }

  getFirstSegmentOfUrl() {
    const firstSegment = this.router.url.split('/')[1];
    return firstSegment ? firstSegment.split('?')[0] : '';
  }

  setQueryToUrl(cityName: string) {
    const navigationExtras: NavigationExtras = {
      queryParams: { city: cityName }
    };
    this.router.navigate([this.getFirstSegmentOfUrl()], navigationExtras);
  }
}
