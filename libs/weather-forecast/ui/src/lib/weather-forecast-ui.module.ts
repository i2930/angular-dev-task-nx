import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './layouts/main/main.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { WeatherForecastLoadingComponent } from './components/weather-forecast-loading/weather-forecast-loading.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [
    MainComponent,
    HeaderComponent,
    FooterComponent,
    WeatherForecastLoadingComponent
  ],
  exports: [MainComponent, WeatherForecastLoadingComponent]
})
export class WeatherForecastUiModule {}
