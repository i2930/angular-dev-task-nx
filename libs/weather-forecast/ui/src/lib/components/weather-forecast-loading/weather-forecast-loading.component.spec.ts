import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherForecastLoadingComponent } from './weather-forecast-loading.component';

describe('WeatherForecastLoadingComponent', () => {
  let component: WeatherForecastLoadingComponent;
  let fixture: ComponentFixture<WeatherForecastLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherForecastLoadingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherForecastLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
