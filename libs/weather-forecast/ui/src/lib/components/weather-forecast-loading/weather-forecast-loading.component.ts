import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nx-test-weather-forecast-loading',
  templateUrl: './weather-forecast-loading.component.html',
  styleUrls: ['./weather-forecast-loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherForecastLoadingComponent implements OnInit {
  @Input() isLoading: boolean | null;

  constructor() {
    this.isLoading = false;
  }

  ngOnInit(): void {
  }

}
