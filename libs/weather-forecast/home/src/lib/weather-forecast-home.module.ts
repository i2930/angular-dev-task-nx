
import { WeatherForecastHomeService } from '@nx-test/weather-forecast/data-access';
import { WeatherForecastDataAccessModule } from '@nx-test/weather-forecast/data-access';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './containers/home/home.component';
import { HomeRoutingModule } from './containers/home/home-routing.module';
import { WeatherForecastFeatureSearchModule } from '@nx-test/weather-forecast/feature-search';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    WeatherForecastDataAccessModule,
    WeatherForecastFeatureSearchModule
  ],
  providers: [WeatherForecastHomeService],
  declarations: [HomeComponent],
})
export class WeatherForecastHomeModule {}
