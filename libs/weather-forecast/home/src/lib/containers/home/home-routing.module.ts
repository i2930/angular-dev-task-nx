import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        redirectTo: 'daily'
      },
      {
        path: 'daily',
        loadChildren: () => import('@nx-test/weather-forecast/feature-daily').then(m =>  m.WeatherForecastFeatureDailyModule)
      },
      {
        path: 'hourly',
        loadChildren: () => import('@nx-test/weather-forecast/feature-hourly').then(m =>  m.WeatherForecastFeatureHourlyModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
