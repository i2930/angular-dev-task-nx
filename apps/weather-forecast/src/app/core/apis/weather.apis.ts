import { environment } from '../../../environments/environment';

const weatherApisPaths = {
	'getCityPath': environment.weatherApi + '/geo/1.0/direct',
	'getWeatherDataPath': environment.weatherApi + '/data/2.5/onecall'
};

export { weatherApisPaths };
