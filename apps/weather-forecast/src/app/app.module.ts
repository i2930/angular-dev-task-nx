import { WeatherForecastUiModule } from './../../../../libs/weather-forecast/ui/src/lib/weather-forecast-ui.module';
import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { weatherApisPaths } from './core/apis/weather.apis';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(
      {},
      {
        metaReducers: !environment.production ? [] : [],
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
        },
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot(),
    WeatherForecastUiModule
  ],
  providers: [
    {
			provide: 'appKey',
			useValue: environment.appKey,
		},
		{
			provide: 'weatherApis',
			useValue: weatherApisPaths,
		},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
