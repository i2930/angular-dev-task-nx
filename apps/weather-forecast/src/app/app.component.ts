import { Component } from '@angular/core';

@Component({
  selector: 'weather-forecast',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'weather-forecast';
}
