import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from 'libs/weather-forecast/ui/src/lib/layouts/main/main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
     loadChildren: () => import('@nx-test/weather-forecast/home').then(m =>  m.WeatherForecastHomeModule)
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
